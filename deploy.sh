ssh-add <(echo "$SSH_PRIVATE_KEY")
# Creating archive
cd ./for_docker/backend/worker/
tar -czf ../build.tgz .
cd ..
echo "Deploying to server"
# create tmp folder
ssh -p $PORT $USER@$HOST "mkdir -p tmp"
# spending archive to remote machine
scp -P $PORT ./build.tgz $USER@$HOST:/tmp
# Deleting previos build
ssh -p $PORT $USER@$HOST "rm -rf /$DESTINATION/*"    
# Extracting archive
ssh -p $PORT $USER@$HOST "tar -xzf /tmp/build.tgz -C /$DESTINATION/"
# delete archive
ssh -p $PORT $USER@$HOST "rm -f /tmp/lbuild.tgz"
# stop previous version of docker container
ssh -p $PORT $USER@$HOST "docker stop certdetectworker_cert_detect_worker_1"
docker build new version
ssh -p $PORT $USER@$HOST "docker-compose -f $DESTINATION/docker-compose.yml up --build -d"
echo "Deployed"